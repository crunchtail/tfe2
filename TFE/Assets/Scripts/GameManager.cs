﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : Bolt.GlobalEventListener
{
    [SerializeField]
    public MoveSample[] players;
    
    BoltEntity jugador;
    BoltEntity servidor;

    spawnPointsManager spawns;

    public bool isReady;

    
    public override void SceneLoadLocalDone(string scene)
    {

        Debug.Log("Diego:escena cargada en red");
        if (BoltNetwork.IsServer)
        {
            IniciarPartida();
        }
        else
        {
            StartCoroutine(iniciarCliente());
        }
        
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    private void DebugInfoPhoton()
    {
        Debug.Log("Network Status: Is server: " + BoltNetwork.IsServer);
        Debug.Log("Network Status: Is client: " + BoltNetwork.IsClient);

    }
    public void IniciarPartida()
    {

        DebugInfoPhoton();


        if (BoltNetwork.IsServer)
        {
            servidor = BoltNetwork.Instantiate(BoltPrefabs.Gamemanager, Vector3.zero, Quaternion.identity);
            isReady = true;
        }       

        Debug.Log("Diego:Iniciando manager de partida");
        spawns = FindObjectOfType<spawnPointsManager>();

       
        Debug.Log("Diego:Encontrado servidor con datos " + servidor.name);

        int nJugadores = servidor.GetState<IGameManager>().NumeroPlayers;
        Debug.Log("Diego:Nº de jugadores conectados" + nJugadores.ToString());

        Debug.Log("Diego:Instanciando player");
        jugador = BoltNetwork.Instantiate(BoltPrefabs.Player_Online, spawns.spawnPoints[nJugadores].position, Quaternion.identity);

        //encontrar la camara en la escena
        FollowTargetSmooth targetCamara = FindObjectOfType<FollowTargetSmooth>();

        ControlRotCamera rotCamera = FindObjectOfType<ControlRotCamera>();
        rotCamera.mianim = jugador.GetComponent<MoveSample>().anim;

        //asignarle el player
        targetCamara.target = jugador.GetComponent<Transform>();

        Debug.Log("Diego:player instanciado " + jugador.name);
        jugador.TakeControl();

        Debug.Log("Diego:Tomando el control de la instancia " + jugador.name);

        var msg = AumentarJugadores.Create();
        msg.Send();     
        
    }

    IEnumerator iniciarCliente()
    {

        GameObject item;
        do
        {
            item = GameObject.FindGameObjectWithTag("Gamemanager");

            if (item != null)   servidor = item.GetComponent<BoltEntity>();

            yield return new WaitForEndOfFrame();
        } while (servidor == null);


        isReady = true;

        IniciarPartida();

    }
        
    public override void OnEvent(AumentarJugadores evnt)
    {
        
        if (!BoltNetwork.IsServer) return;
        Debug.Log("Evento de player creado");
        servidor.GetState<IGameManager>().NumeroPlayers++;

        
    }

}
