﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// This script moves the character controller forward
// and sideways based on the arrow keys.
// It also jumps when pressing space.
// Make sure to attach a character controller to the same game object.
// It is recommended that you make only one call to Move or SimpleMove per frame.

public class MoveSample : Bolt.EntityBehaviour<IPlayer>
{
    CharacterController characterController;

    public float speed = 6.0f;
    float running = 1;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float rot_speed = 3;
    private Vector3 moveDirection = Vector3.zero;

    public Animator anim;
    public Transform posicionObjetos;
    AimingCamera shoot;
    bool jump;
    float xRoot;


    void Start()
    {
        characterController = GetComponent<CharacterController>();
        if (entity.IsOwner)
        {
            FindObjectOfType<AimingCamera>().posicionBala = posicionObjetos.Find("posicionApuntado");
        }
        shoot = FindObjectOfType<AimingCamera>().GetComponent<AimingCamera>();
    }

    private void Update()
    {
        
        if (!entity.IsOwner)
        {
            if (characterController != null)
            {
                characterController.enabled = false;
                
            }

            anim.SetBool("isRunning", state.isRunning);
            anim.SetBool("isGrounded", state.isGrounded);
            anim.SetFloat("Horizontal", state.Horizontal);
            anim.SetFloat("GradosX", state.GradosX);
            anim.SetFloat("Vertical", state.Vertical);
            

            return;
        }
        
        #region Inputs
        //Inputs
        if (characterController.isGrounded)
        {
            //Animator
            anim.SetBool("isGrounded", true);
            anim.SetFloat("Vertical", Input.GetAxis("Vertical"));
            
            anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));

            //Movimiento
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;

            if (Input.GetButton("Fire3"))
            {
                anim.SetBool("isRunning", true);
                running = 2;
            }
            else
            {
                anim.SetBool("isRunning", false);
                running = 1;
            }

            if (Input.GetButton("Fire2"))
            {

                anim.SetLayerWeight(1, 1);
            }
            else
            {
                anim.SetLayerWeight(1, 0);
            }
            if (Input.GetButton("Fire1"))
            {
                if (!anim.GetCurrentAnimatorStateInfo(2).IsTag("Shoot"))
                {
                    anim.SetTrigger("Shoot");
                    shoot.Disparar();
                }
                
            }
            if (Input.GetButtonDown("Jump")) jump = true;
            
        }
        else
        {
            jump = false;
            anim.SetBool("isRunning", false);
            running = 1;
            anim.SetBool("isGrounded", false);
            anim.SetLayerWeight(1, 0);
        }
        xRoot = Input.GetAxis("Mouse X");

        state.isRunning = anim.GetBool("isRunning");
        state.isGrounded = anim.GetBool("isGrounded");
        state.Horizontal = anim.GetFloat("Horizontal");
        state.Vertical = anim.GetFloat("Vertical");
        state.GradosX = anim.GetFloat("GradosX");
        #endregion

    }

    void FixedUpdate()
    {
        if (!entity.IsOwner) return;
        if (characterController.isGrounded)
        {
           
            // We are grounded, so recalculate
            // move direction directly from axes

            
            
            
            
            

            if (jump)
            {
                moveDirection.y = jumpSpeed;
                
            }
        }
        else
        {
            anim.SetBool("isGrounded", false);
        }
        //ROTACION
        transform.Rotate(0f, xRoot * rot_speed, 0);


        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(moveDirection * running * Time.deltaTime);
    }
    public override void Attached()
    {
        state.SetTransforms(state.PosAndRoot, transform);
        state.SetAnimator(anim);

        
    }

    
}