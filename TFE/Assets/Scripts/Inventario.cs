﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventario : MonoBehaviour
{
    Objeto[] objetosInventario;
    
    public Image[] objetos;
    public Image seleccion;
    int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            CambiarObjeto(1);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            CambiarObjeto(-1);
        }

        seleccion.transform.position = Vector3.Lerp(seleccion.transform.position, objetos[index].transform.position, 0.1f);
    }
    void CambiarObjeto(int cambioArma)
    {
        if(index == 4 - 1 && cambioArma > 0)
        {
            index = 0;
        }
        else if (index == 0 && cambioArma < 0)
        {
            index = 4 - 1;
        }
        else
        {
            index += cambioArma;
        }

        //seleccion.rectTransform.position = Vector2.Lerp(seleccion.rectTransform.position, objetos[index].rectTransform.position, 0.05f);
        
    }
}
