﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingCamera : MonoBehaviour
{
    public Transform camera, mirilla, posicionBala;
    public LayerMask mascara;
    public GameObject particulas;
    RaycastHit hit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        Ray rayo = new Ray();
        rayo.direction = camera.transform.forward;
        if(Physics.Raycast(camera.position, camera.forward, out hit, 50, mascara))
        {
            mirilla.position = hit.point;
        }
        else
        {
            mirilla.localPosition = new Vector3(0, 0, 50);
        }

        posicionBala.LookAt(mirilla);
        
        
    }
    public void Disparar()
    {
        if (Physics.Raycast(posicionBala.position, posicionBala.forward, out hit, 50, mascara))
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Debug.Log(hit.collider.gameObject);
                Instantiate(particulas, hit.point, Quaternion.identity);
            }

        }
    }
}
